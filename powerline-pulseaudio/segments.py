from pulsectl import Pulse

_pulse = Pulse("powerline")

def volume(pl, format="{}%", sink=0):
    volume = round(_pulse.sink_list()[sink].volume.value_flat * 100)
    return format.format(volume)

def mute(pl, text_on="MUTE", text_off=None, sink=0):
    if _pulse.sink_list()[sink].mute == 0:
        return text_off
    else:
        return text_on

