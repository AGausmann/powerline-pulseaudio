from setuptools import setup

setup(name='powerline-pulseaudio',
      version='0.1',
      description='Powerline segments and selectors that provide information about the PulseAudio server.',
      url='https://gitlab.com/AGausmann/powerline-pulseaudio',
      author='Adam Gausmann',
      author_email='adam@nonemu.ninja',
      license='GPLv3',
      packages=['powerline-pulseaudio'],
      install_requires=[
          'powerline-status',
          'pulsectl'
      ],
      zip_safe=False)

